=============================
Data Exchange aRgument Parser
=============================

Summary
=======

The Data Exchange aRgument Parser, abbreviated as *DERP*, is a specialized argument parsing library for structured data input to Python programs. We expect this library to find application within automation pipelines containing scripts that require large amounts of application state in order to run.

DERP enables passing rich, type checked data structures into Python programs in a uniform way, enabling simpler pipelines with built-in data validation at each step.

Developing
==========

Set up your development environment:

.. code-block:: shell
    poetry install
