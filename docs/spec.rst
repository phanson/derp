=============================
Data Exchange aRgument Parser
=============================

Summary
=======

The Data Exchange aRgument Parser, abbreviated as *DERP*, is a specialized argument parsing library for structured data input to Python programs. We expect this library to find application within automation pipelines containing scripts that require large amounts of application state in order to run.

DERP enables passing rich, type checked data structures into Python programs in a uniform way, enabling simpler pipelines with built-in data validation at each step.


Background
----------

Automation pipelines often include scripts that must ingest application state from multiple sources but do not have access to a global state repository. For instance, multiple scripts that are executed sequentially as part of a Jenkins pipeline may run on different executor nodes or after long delays relative to each other and therefore they cannot rely on node-local storage. In this instance the scripts must operate on serializable state that is mediated by the Jenkins pipeline itself.

A naive solution to the serializable state problem is to add argument definitions to each script. But then how to keep similar argument definitions consistent across multiple scripts? What if a given script requires 10-20 arguments, but some of them are only passed conditionally? Your pipeline templating will quickly become inscrutable to the point of inducing user error.

DERP is a solution to both problems: you define structured input data as a set of Python data types, then callers supply a document in JSON format as the only argument to your script. DERP will read the arguments document, validate the contents against the acceptable set of data types (via Pydantic), and pass the resulting typed arguments object to your code.


Goals
-----

Here are the things that we intend to achieve with this project:

  * Provide an open source library-based solution to the serializable state problem for automation pipelines (described above).
  * Rapidly reach production quality quickly and freeze feature set in order to contain scope.


Non-Goals
---------

These things are specifically **not** goals of this project and will
remain out of scope:

  * Improve the state of typed argument parsing generally. There are already existing popular libraries such as `click` that address general command line argument parsing and associated utilities.
  * Support for non-pipeline use cases.


Design
======

Because DERP is intended to help engineers manage a set of related scripts that share application state and therefore will probably share data type definitions, scripts are required to use a subcommand definition pattern. In addition to the main entry point (which should simply call `dispatch`), a number of Python functions may be defined and then decorated with `@command` to denote that that function contains the body of a callable subcommand. For instance, here is an example of a tool with one subcommand that takes no arguments:

.. code-block:: python
    :caption: frob.py
    :name: frob-py
  #!/usr/bin/env python3
  import derp

  @derp.command
  def woozle():
      """Frob the woozle."""
      print("Frobbed the woozle.")

  if __name__ == "__main__":
      derp.dispatch()

Use `@arguments` to define the argument object type that your command will accept:

.. code-block:: python
    :caption: quux.py
    :name: quux-py
  #!/usr/bin/env python3
  from pydantic import BaseModel
  import derp

  class MeerpArgs(BaseModel):
      foo: int
      bar: str
      baz: dict[str, int | str]

  @derp.command
  def meerp(args: MeerpArgs):
      """Execute the Meerp protocol for Quux."""
      print(args)

  if __name__ == "__main__":
      derp.dispatch()

DERP will automatically generate interactive documentation for available subcommands and their accepted argument data types. Help text will be accessible when the entry point or subcommand is invoked with the special `--help` argument.

.. code-block::
    :caption: Sample interactive help for :ref:`frob-py`
  $ ./frob.py --help
  Usage: ./frob.py <command> [<argument_file>] [--help]

  Available commands:
    woozle    Frob the woozle.

  $ ./frob.py woozle --help
  Usage: ./frob.py woozle [--help]

  Frob the woozle.

.. code-block::
    :caption: Sample interactive help for :ref:`quux-py`
  $ ./quux.py --help
  Usage: ./quux.py <command> [<argument_file>] [--help]

  Available commands:
    meerp     Execute the Meerp protocol for Quux.
  
  $ ./quux.py meerp --help
  Usage: ./quux.py meerp <argument_file> [--help]

  Execute the Meerp protocol for Quux.

  Argument schema:
    foo: int
    bar: str
    baz: dict[str, int | str]

Finally, as a convenience for users who want to use or test their scripts via shell pipelining, the user can supply a dash character (`'-'`) as the arguments document filename and DERP will attempt to read the JSON arguments from `stdin`. Note that use of this feature my interfere with reading other input from `stdin` and therefore is not recommended for interactive scripts.


Other Considerations
====================

Here are a few of the things we considered but decided against, at least
for now:

  * Overloading: the ability to specify multiple input files on the command line (from most general to most specific) and allow the contents of later files to selectively override contents from earlier files, similar to a recursive dictionary update.

