# read version from installed package
from importlib.metadata import version

__version__ = version("derp")

# populate package namespace
from derp.decorators import command
from derp.dispatcher import dispatch
