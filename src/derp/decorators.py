"""
Decorators for defining commands.
"""
from collections.abc import Callable
from typing import ParamSpec, TypeVar

from derp.registry import get_registry

T = TypeVar("T")
P = ParamSpec("P")


def command(func: Callable[P, T]) -> Callable[P, T]:
    """
    Decorator for registering a command.
    """
    get_registry().add(func)
    return func
