"""
Responsible for dispatching command line invocations to the appropriate command.
"""
import sys
from copy import copy

from pydantic import parse_file_as

from derp.registry import Command, NoSuchCommandError, get_registry


def usage(script_name: str, command: Command | None = None) -> str:
    """General script invocation documentation generator."""
    usage = f"Usage: {script_name} "
    if command is None:
        usage += "<command> [<argument_file>]"
    else:
        usage += command.name
        if command.argument_type is not None:
            usage += " <argument_file>"
    usage += " [--help]\n"
    return usage


def general_help() -> str:
    """List available commands."""
    command_table = "Available commands:\n"
    all_commands = get_registry().all()
    for cmd in all_commands:
        command_table += f"\t{cmd.name}\t{cmd.help}\n"
    command_table += "\n"
    return command_table


def command_help(command: Command) -> str:
    """Show command-specific info and argument schema."""
    command_help = command.help
    if command.argument_type is not None:
        command_help += "\n\nArgument schema:\n"
        command_help += repr(command.argument_type.schema())
    command_help += "\n"
    return command_help


def dispatch(argv: list[str] | None = None) -> int:
    # fall back to default if unset
    if argv is None:
        argv = copy(sys.argv)

    # set help flag and remove
    if is_help_invocation := "--help" in argv:
        argv.remove("--help")

    # identify command and arguments filename, if any
    command_name = argv[1] if len(argv) > 1 else None
    arguments_filename = argv[2] if len(argv) > 2 else None

    # early error if command does not exist
    command = None
    if command_name is not None:
        try:
            command = get_registry().get(command_name)
        except NoSuchCommandError:
            print(f"Unknown command: {command_name}")
            return 1

    # display help text of appropriate specificity and exit
    if is_help_invocation or command is None:
        print(usage(argv[0], command))
        if command is not None:
            print(command_help(command))
        else:
            print(general_help())
        return 0

    # parse arguments file if required by command
    args = None
    if command.argument_type is not None:
        if arguments_filename is not None:
            # parse and validate arguments file
            args = parse_file_as(command.argument_type, arguments_filename)
            # invoke target function with arguments
            return command.callable(args)
        else:
            # missing required arguments file
            raise RuntimeError(f"Arguments file required for {command.name}")
    else:
        if arguments_filename is not None:
            # arguments file provided when not needed
            raise RuntimeError(
                f"Unexpected argument {arguments_filename}; {command.name} does not require an arguments file."
            )
        else:
            # invoke target function with no arguments
            return command.callable()
