"""
Responsible for managing the command registry.
"""
from collections.abc import Callable
from typing import get_type_hints

from pydantic import BaseModel


class ArityError(Exception):
    pass


class Command:
    """
    Encapsulates information about an individual command function and its metadata.
    """

    def __init__(self, func: Callable):
        self.callable = func

    @property
    def name(self) -> str:
        """Normalized command name."""
        return getattr(self.callable, "__name__", "unknown")

    @property
    def help(self) -> str:
        """Help text to display for command."""
        return getattr(self.callable, "__doc__", "")

    @property
    def argument_type(self) -> BaseModel | None:
        """Data type of command argument."""
        argument_types = get_type_hints(self.callable)
        if len(argument_types) > 1:
            raise ArityError(f"Got command with {len(argument_types)} arguments; expected 0 or 1.")
        if len(argument_types) == 0:
            return None
        return list(argument_types.values())[0]


class NoSuchCommandError(Exception):
    pass


class Registry:
    """
    Tracks all of the commands that have been registered.
    """

    def __init__(self):
        self.commands = {}

    def add(self, func: Callable) -> None:
        """Add a command to the registry."""
        sc = Command(func)
        self.commands[sc.name] = sc

    def has(self, name: str) -> bool:
        """Return True iff there is a command with the given name."""
        return name in self.commands

    def get(self, name: str) -> Command:
        """Retrieve the command object associated with the given name."""
        try:
            return self.commands[name]
        except KeyError as ex:
            raise NoSuchCommandError(name) from ex

    def all(self) -> list[Command]:
        """Retrieve the list of all registered commands."""
        return sorted(self.commands.values(), key=lambda x: x.name)


_REGISTRY = None


def get_registry():
    """Obtain a reference to the global registry."""
    global _REGISTRY
    if _REGISTRY is None:
        _REGISTRY = Registry()
    return _REGISTRY
